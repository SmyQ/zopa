﻿using System.Collections.Generic;
using Zopa.BLO;
using Zopa.Models;

namespace Zopa.Interfaces
{
    public interface IRateAlgorithm
    {
        QuoteResultModel CalculateRate(int requested, IEnumerable<LenderModel> lenders);
    }
}