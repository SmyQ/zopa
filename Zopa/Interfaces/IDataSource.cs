using System.Collections.Generic;
using Zopa.BLO;

namespace Zopa
{
    public interface IDataSource<T>
    {
        List<T> GetAllEntities();
    }
}