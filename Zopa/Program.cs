﻿using System;
using System.Globalization;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Zopa.BLO;
using Zopa.Interfaces;

namespace Zopa
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string marketFile = args[0];
            int loanAmount = int.Parse(args[1]);

            var container = new WindsorContainer();
            container.Install(FromAssembly.This());
            IRateAlgorithm rateAlgorithm = container.Resolve<IRateAlgorithm>();
            IDataSource<LenderModel> dataSource = container.Resolve<IDataSource<LenderModel>>( new { csvFilePath = marketFile });
            
            var lenders = dataSource.GetAllEntities();
            var result = rateAlgorithm.CalculateRate(loanAmount, lenders);

            var currencySymbol = CultureInfo.GetCultureInfo("en-GB").NumberFormat.CurrencySymbol;
            Console.WriteLine("RequestedAmount: {0}", result.Requested);
            Console.WriteLine("Rate: {0:0.0}%", result.AnnualRate* 100);
            Console.WriteLine("Monthly repayment: {0:.00}", result.MonthlyPayment);
            Console.WriteLine("Total repayment: {0:.00}", result.TotalRepayment);
        }
    }
}
