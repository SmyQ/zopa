﻿namespace Zopa.BLO
{
    public class LenderModel
    {
        public string Lender { get; set; }
        public double Rate { get; set; }
        public double Available { get; set; }
    }
}
