using Zopa.BLO;

namespace Zopa.Services
{
    public class OfferModel
    {
        public int AmountAssigned { get; set; }
        public LenderModel Lender { get; set; }
    }
}