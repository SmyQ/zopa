﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zopa.Models
{
    public class QuoteResultModel
    {
        public int Requested { get; set; }
        public double TotalRepayment { get; set; }
        public double MonthlyPayment { get; set; }
        public double AnnualRate { get; set; }
    }
}
