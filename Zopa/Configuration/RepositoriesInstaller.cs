﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Zopa.BLO;
using Zopa.Interfaces;
using Zopa.Services;

namespace Zopa.Configuration
{
    public class RepositoriesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDataSource<LenderModel>>().ImplementedBy<CsvLenderDataSource>());
            container.Register(Component.For<IRateAlgorithm>().ImplementedBy<RateAlgorithm>());
        }
    }
}