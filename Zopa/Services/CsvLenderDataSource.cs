﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Zopa.BLO;

namespace Zopa.Services
{
    public class CsvLenderDataSource: IDataSource<LenderModel>
    {
        private readonly string csvFilePath;

        public CsvLenderDataSource(string csvFilePath)
        {
            this.csvFilePath = csvFilePath;
        }

        public List<LenderModel> GetAllEntities()
        {
            var csv = new CsvReader(new StreamReader(csvFilePath));
            var lenders = csv.GetRecords<LenderModel>();

            return lenders.ToList();
        }

    }
}
