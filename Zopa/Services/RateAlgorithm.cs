﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zopa.BLO;
using Zopa.Interfaces;
using Zopa.Models;

namespace Zopa.Services
{
    public class RateAlgorithm: IRateAlgorithm
    {
        private readonly double monthsLendingPeriod = 36;

        public QuoteResultModel CalculateRate(int requested, IEnumerable<LenderModel> lenders)
        {
            IEnumerable<OfferModel> offersAssigned = SelectOfferesWithLowestIntrestRates(requested, lenders);
            if (!requested.Equals(offersAssigned.Sum(offer => offer.AmountAssigned)))
            {
                throw new Exception("Market does not have sufficient offers from lenders to satisfy the loan, Requested Loan: {0}");
            }

            double totalRepayment = offersAssigned.Sum(offer => CalculateTotalLoanForOffer(offer));
            double annualRate = offersAssigned.Sum(offer => (offer.AmountAssigned/(double)requested)*offer.Lender.Rate);

            QuoteResultModel quoteResult = new QuoteResultModel()
            {
                Requested = requested,
                TotalRepayment = totalRepayment,
                MonthlyPayment = totalRepayment/monthsLendingPeriod,
                AnnualRate = annualRate
            };

            return quoteResult;
        }

        /// <summary>
        /// From all lenders select offers with a lowest possible intrest rates
        /// </summary>
        /// <param name="requested"></param>
        /// <param name="lenders"></param>
        /// <returns></returns>
        private IEnumerable<OfferModel> SelectOfferesWithLowestIntrestRates(int requested,
            IEnumerable<LenderModel> lenders)
        {
            int totalAmountAssigned = 0;
            lenders = lenders.OrderBy(lender => lender.Rate);

            foreach (LenderModel lender in lenders)
            {
                if (totalAmountAssigned.Equals(requested)) yield break;

                OfferModel offer = new OfferModel()
                {
                    AmountAssigned = (int) Math.Min(lender.Available, requested - totalAmountAssigned),
                    Lender = lender
                };
                totalAmountAssigned += offer.AmountAssigned;
                yield return offer;
            }
        }

        /// <summary>
        /// Loan amortization formula, more details: https://en.wikipedia.org/wiki/Amortization_calculator
        /// </summary>
        /// <param name="offer"></param>
        /// <returns></returns>
        private double CalculateTotalLoanForOffer(OfferModel offer)
        {
            double totalLoan = 0.0,
                loanInitialValue = offer.AmountAssigned,
                monthlyLoanRate = offer.Lender.Rate/12,
                totalNumberOfPayments = monthsLendingPeriod,
                comboundIntrestRate = Math.Pow(1 + monthlyLoanRate, totalNumberOfPayments);

            totalLoan = (monthlyLoanRate*loanInitialValue*comboundIntrestRate)/(comboundIntrestRate - 1);

            return totalLoan * totalNumberOfPayments;
        }
    }
}
